# GetKB57796VMS

Get-KB57796VMS

Get all VMs affected by VMware Tools Version 10.3.0 Recall

[VMware Tools Version 10.3.0 Recall and Workaround Recommendations (57796)](https://kb.vmware.com/s/article/57796)


# Get a list of affected VMs

- download 
- connect to vCenter/ESXi host  Connect-VIServer -Server vcenter.corp.com
- Unblock-File .\getKB57796VMS.ps1
- Execute 

.\Get-KB57796VMS.ps1