##config

## VMware Tools Version 10.3.0 Recall and Workaround Recommendations (57796)
## https://kb.vmware.com/s/article/57796

$VMhostVersion      = "6.5.0"
$VMHardwareVersion  = "vmx-13"
$ToolsVersion       = "10.3.0"

#OSVersion=Windows 8/Windows Server 2012 or higher guest OS
$OSVersion=@("windows8Guest","windows9Guest","windows8Server64Guest","windows9Server64Guest")



#check VMware.PowerCLI Modules
if (-not (Get-Module -ListAvailable -Name "VMware.PowerCLI"))
    {Write-Host 'VMWare "VMware.PowerCLI" Module nicht installiert... 
    
    ' -ForegroundColor Red

    Write-Host -NoNewline 'Try: '
    Write-Host -NoNewline -ForegroundColor Green  'Install-Module -Name "VMware.PowerCLI"
    '
    break
    }

#test connection
if (-not ($global:DefaultVIServer))
    {Write-Host 'Not connected to vCenter or ESXi-Host   
    
    ' -ForegroundColor Red
    Write-Host -NoNewline 'Try: '
    Write-Host -NoNewline -ForegroundColor Green  'Connect-VIServer -Server vcenter.corp.com
    '
    break
    }

#start
$affecedthost = Get-VMHost |Where-Object Version -EQ $VMhostVersion
$affectedVMs = $affecedthost |Get-VM |Where-Object {$_.HardwareVersion -eq $VMHardwareVersion -and $_.Guest.ConfiguredGuestId -in $OSVersion -and $_.Guest.ToolsVersion -eq $ToolsVersion}

#Out Affected VMs
$affectedVMs | Sort-Object | Get-View | Format-Table -AutoSize Name,@{Name="Hardware Version"; Expression={$VMHardwareVersion}},@{Name="Tools Version"; Expression={$_.Guest.ToolsVersion}}


if ($affectedVMs -eq $null) {Write-Host -ForegroundColor Cyan '
No VMs are affected ...
'}

